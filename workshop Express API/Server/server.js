const express = require('express');
const mongoose = require('mongoose');
// 3 ประสาน Middleware

const morgan = require('morgan'); //แสดงlog ใน console
const bodyParser = require('body-parser'); // รับส่งข้อมูล json จากหน้าบ้านไปหลังบ้าน
const cors = require('cors');
const { readdirSync } = require('fs')
const connectDB = require('./Config/db')

// Routes
// const authRoutes = require('./Routes/auth')
// const productRoutes = require('./Routes/product')


// app
const app = express();

// db

connectDB()
// middleware
app.use(morgan("dev"))
app.use(bodyParser.json({ limit: "10mb"}))

// Route 1
app.get('/register', (req, res) => {
    // code
    console.log("Hello Register");
    res.send("Hello Goodmornig")
})

// Route 2
// app.use('/api', authRoutes)
// app.use('/api', productRoutes)

// Route 3
readdirSync('./Routes').map((r) => app.use('/api', require('./Routes/' + r)))
// r คือ ชื่อไฟล์ใน folder Routes ทุกตัว


app.listen(5000, () => {
    console.log('Server is Running on Port 5000');
})

